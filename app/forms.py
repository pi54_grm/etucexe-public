from flask_wtf import FlaskForm
from flask_wtf.file import FileField, FileRequired
from wtforms import StringField, PasswordField, BooleanField, SubmitField, TextAreaField, SelectMultipleField,widgets, validators
from wtforms.validators import ValidationError, DataRequired, Email, EqualTo,Length
from app import mongo, app
class LoginForm(FlaskForm):
    username = StringField('Username', validators=[DataRequired()])
    password = PasswordField('Password', validators=[DataRequired()])
    remember_me = BooleanField('Remember Me')
    submit = SubmitField('Sign In')

class RegistrationForm(FlaskForm):
    username = StringField('Username', validators=[DataRequired()])
    password = PasswordField('Password', validators=[DataRequired()])
    password2 = PasswordField(
        'Repeat Password', validators=[DataRequired(), EqualTo('password')])
    submit = SubmitField('Register')

    def validate_username(self, username):
        user = mongo.db.users.find_one({'_id': username.data});
        if user is not None:
            raise ValidationError('Please use a different username.')

class GamesForm(FlaskForm):
    name = StringField('Name of Game', validators=[DataRequired()])
    file = FileField('Archive',validators=[DataRequired()])
    exe = StringField('Exe file', validators=[DataRequired()])
    discription = StringField('Discription', validators=[DataRequired()])
    language = StringField('Language', validators=[DataRequired()])
    year = StringField('Year', validators=[DataRequired()])
    submit = SubmitField('Add Game')
class MultiCheckboxField(SelectMultipleField):
    widget = widgets.ListWidget(prefix_label=False)
    option_widget = widgets.CheckboxInput()
class AchievementForm(FlaskForm):
    name = StringField('Name of Game', validators=[DataRequired()])
    exp = StringField('Ex.Points', validators=[DataRequired()])
    _choices = []
    with app.app_context():
        games = mongo.db.games.find()
        for game in games:
                _choices.append((str(game['_id']), game['name']))
    games = MultiCheckboxField('Games', choices = _choices, coerce=str)
    submit = SubmitField('AddAchievement')
