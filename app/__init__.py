from flask import Flask
from flask_login import LoginManager
from flask_pymongo import PyMongo
app = Flask(__name__)
login = LoginManager(app)
login.login_view = 'login'
app.config.update(dict(
    SECRET_KEY="diksas",
    WTF_CSRF_SECRET_KEY="rofl"
))
app.config['MONGO_DBNAME'] = 'etucexe'
app.config['MONGO_URI'] = '--Connection String--'
lm = LoginManager()
lm.init_app(app)
lm.login_view = 'login'

mongo = PyMongo(app)
from app import routes
