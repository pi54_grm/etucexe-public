from app import app, mongo
from flask import render_template, flash, redirect, url_for, request, jsonify
from app.models import User
from app.forms import LoginForm, RegistrationForm, GamesForm, AchievementForm
from flask_login import login_user,login_required, current_user,  logout_user
from werkzeug.security import generate_password_hash, check_password_hash
from werkzeug.utils import secure_filename
from flask.views import MethodView
from bson import DBRef, ObjectId
import os

class IndexAPI(MethodView):
    def get(self):
        return render_template("index.html")
class GamesAPI(MethodView):
    def get(self, game_name):
        if game_name is None:
            games = mongo.db.games.find().sort([("name", 1)])
            return render_template("games.html", games = games,title = 'Games')
        game = mongo.db.games.find_one_or_404({'name': game_name.replace("_", " ")})
        user = mongo.db.users.find_one({'_id': current_user.get_id()})
        if user is not None and game['_id'] in user['games']:
            listGame = True;
        else:
            listGame = False
        return render_template("game.html", gamename=game['file'], exename=game['exe'], name=game['name'], listGame = listGame,title = game['name'])
class RegisterAPI(MethodView):
    def get(self):
        if current_user.is_authenticated:
            return redirect(url_for('index'))
        form = RegistrationForm()
        return render_template("register.html", form=form,title = 'Registration')
    def post(self):
        form = RegistrationForm()
        if form.validate_on_submit():
            user = mongo.db.users
            user.insert( { '_id': form.username.data, 'password':generate_password_hash(form.password.data), 'games':[], 'lvl':1, 'exp':0, 'achievements':[]})
            flash('Congratulations, you are now a registered user!')
            return redirect(url_for('index'))
        return render_template("register.html", form=form,title = 'Registration')

class LoginAPI(MethodView):
    def get(self):
        if current_user.is_authenticated:
            return redirect(url_for('index'))
        form = LoginForm()
        return render_template('login.html', title='Sign In', form=form)
    def post(self):
        form = LoginForm()
        if form.validate_on_submit():
            user = mongo.db.users.find_one({'_id': form.username.data})
            if user is None or not check_password_hash(user['password'], form.password.data):
                flash('Invalid username or  password')
                return redirect(url_for('login'))
            login_user(User(user['_id']), remember=form.remember_me.data)
            return redirect(url_for('index'))
        return render_template('login.html', title='Sign In', form=form)

class UserAPI(MethodView):
    def get(self,username):
        check_lvl(username)
        check_achievements(username)
        user = mongo.db.users.find_one_or_404({'_id': username})
        max_exp = 1000 * ((1.1)**user['lvl'])
        if user['lvl'] == 1:
            min_exp = 0
        else:
            min_exp = 1000 * ((1.1)**(user['lvl']-1))
        color = ['orange', 'yellow', 'olive', 'green', 'teal']
        games = mongo.db.games.find({ '_id':  {"$in": user['games']} })
        achievements = mongo.db.achievements.find({ '_id':  {"$in": user['achievements']} })
        return render_template("user.html", user = user, games = games, color = color, max_exp = max_exp, min_exp = min_exp, achievements = achievements,title = username)

class AddGameAPI(MethodView):
    def get(self):
        if current_user.get_id() == "vertilo":
            form = GamesForm()
            return render_template("add_game.html", form=form)
        return redirect(url_for('index'))
    def post(self):
        form = GamesForm()
        print(form.validate_on_submit())
        if form.validate_on_submit():
            f = form.file.data
            f.save(os.path.join('app/static/games/', f.filename))
            game = mongo.db.games
            game.insert({'name': form.name.data, 'file':'games/'+form.file.data.filename, 'exe':form.exe.data, 'year': form.year.data,'language': form.language.data,'discription': form.discription.data})
            return redirect(url_for('index'))
        return render_template("add_game.html", form=form)
class AddAchievementAPI(MethodView):
    def get(self):
        if current_user.get_id() == "vertilo":
            form = AchievementForm()
            return render_template("add_achievement.html", form=form)
        return redirect(url_for('index'))
    def post(self):
        form = AchievementForm()
        if form.validate_on_submit():
            mongo.db.achievements.insert({'name': form.name.data, 'exp': int(form.exp.data), 'games': list(map(ObjectId, form.games.data))})
            check_achievements(current_user.get_id())
            return redirect(url_for('index'))
        return render_template("add_achievement.html", form=form)

class AdminAPI(MethodView):
    def get(self, object, id):
        achievements = mongo.db.achievements.find()
        games = mongo.db.games.find()
        a_games = mongo.db.games.find()
        if object is None or id is None:
            return render_template("admin.html", games = games, achievements = achievements, a_games = a_games)
        if object == "achievement":
            form = AchievementForm()
            a = mongo.db.achievements.find_one({'_id':ObjectId(id)})
            form.name.data = a['name']
            form.exp.data = a['exp']
            form.games.data = list(map(str, a['games']))
            return render_template("edit_achievement.html", form=form)
        if object == "game":
            form = GamesForm()
            a = mongo.db.games.find_one({'_id':ObjectId(id)})
            form.name.data = a['name']
            form.file.data = 'sdsd'
            form.exe.data = a['exe']
            form.discription.data = a['discription']
            form.language.data  = a['language']
            form.year.data = a['year']
            return render_template("edit_game.html", form=form)
        if object == "game_delete":
            game = mongo.db.games.find_one({'_id': ObjectId(id)})
            for a in achievements:
                if game['_id'] in a['games']:
                    a['games'].remove(game['_id'])
                    mongo.db.achievements.save(a)
            users = mongo.db.users.find()
            for u in users:
                if game['_id'] in u['games']:
                    u['games'].remove(game['_id'])
                    mongo.db.users.save(u)
            mongo.db.games.remove({'_id': ObjectId(id)})
            games = mongo.db.games.find()
            return redirect(url_for('admin'))
        if object == "achievement_delete":
            achievement = mongo.db.achievements.find_one({'_id': ObjectId(id)})
            users = mongo.db.users.find()
            for u in users:
                if achievement['_id'] in u['achievements']:
                    u['achievements'].remove(achievement['_id'])
                    mongo.db.users.save(u)
            mongo.db.achievements.remove({'_id': ObjectId(id)})
            return redirect(url_for('admin'))
    def post(self, object, id):
        achievements = mongo.db.achievements.find()
        games = mongo.db.games.find()

        if object == "achievement":
            form = AchievementForm()
            if form.validate_on_submit():
                a = mongo.db.achievements.find_one({'_id':ObjectId(id)})
                a['name'] = form.name.data
                a['exp'] = form.exp.data
                a['games'] = list(map(ObjectId, form.games.data))
                mongo.db.achievements.save(a)
                return redirect(url_for('admin'))
            return render_template("edit_achievement.html", form=form)
        if object == "game":
            form = GamesForm()
            if form.validate_on_submit():
                a = mongo.db.games.find_one({'_id':ObjectId(id)})
                a['name'] = form.name.data
                if form.file.data is not None:
                    form.file.data.save(os.path.join('app/static/games/', form.file.data.filename))
                    os.remove(os.path.join('app/static/', a['file']))
                    a['file'] = 'games/'+form.file.data.filename
                a['exe'] = form.exe.data
                a['discription'] = form.discription.data
                a['language'] = form.language.data
                a['year'] = form.year.data
                mongo.db.games.save(a)
                return redirect(url_for('admin'))
            return render_template("edit_game.html", form=form)
app.add_url_rule('/', view_func=IndexAPI.as_view('index'), methods=['GET'])
app.add_url_rule('/game/', defaults={'game_name': None}, view_func=GamesAPI.as_view('games'), methods=['GET'])
app.add_url_rule('/game/<game_name>', view_func=GamesAPI.as_view('game'), methods=['GET'])
app.add_url_rule('/register', view_func=RegisterAPI.as_view('register'), methods=['GET','POST'])
app.add_url_rule('/login', view_func=LoginAPI.as_view('login'), methods=['GET','POST'])
app.add_url_rule('/user/<username>', view_func=UserAPI.as_view('user'), methods=['GET'])
app.add_url_rule('/add_game', view_func=AddGameAPI.as_view('add_game'), methods=['GET','POST'])
app.add_url_rule('/add_achievement', view_func=AddAchievementAPI.as_view('add_achievement'), methods=['GET','POST'])
app.add_url_rule('/admin', defaults={'object': None, 'id': None},view_func=AdminAPI.as_view('admin'), methods=['GET', 'POST'])
app.add_url_rule('/admin/<object>/<id>', view_func=AdminAPI.as_view('edit'), methods=['GET', 'POST'])
@app.route('/_complete')
def add_numbers():
    _game = request.args.get('game')
    user = mongo.db.users.find_one({'_id': current_user.get_id()})
    game = mongo.db.games.find_one({'name': _game})
    if game['_id'] in user['games']:
        user['games'].remove(ObjectId(game['_id']))
        result = 'positive'
    else:
        user['games'].append(ObjectId(game['_id']))
        result = 'negative'
    mongo.db.users.save(user)
    check_achievements(current_user.get_id())
    check_lvl(current_user.get_id())
    return jsonify(result=result)


@app.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('login'))

def check_achievements(username):
    achievements = mongo.db.achievements.find()
    user = mongo.db.users.find_one({'_id':username})
    games = mongo.db.games.find({'_id':{'$in':user['games']}})
    user['achievements'].clear()
    user['exp'] = 0
    for a in achievements:
        if set(a['games']).issubset(user['games']):
            user['achievements'].append(a['_id'])
            user['exp'] += a['exp']
    mongo.db.users.save(user)

def check_lvl(username):
    user = mongo.db.users.find_one({'_id':username})
    max_exp = 1000 * ((1.1)**user['lvl'])
    if user['lvl'] == 1:
        min_exp = 0
    else:
        min_exp = 1000 * ((1.1)**(user['lvl']-1))
    while True:
        if max_exp <= ( min_exp + user['exp'] ):
            user['lvl']+=1
        mongo.db.users.save(user)
        return
