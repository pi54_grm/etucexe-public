from werkzeug.security import check_password_hash
from app import app, lm, mongo
class User():

    def __init__(self, username):
        self.username = username

    def is_authenticated(self):
        return True

    def is_active(self):
        return True

    def is_anonymous(self):
        return False

    def get_id(self):
        return str(self.username)

    @staticmethod
    def validate_login(password_hash, password):
        return check_password_hash(password_hash, password)
@lm.user_loader
def load_user(username):
    u = mongo.db.users.find_one({"_id": username})
    if not u:
        return None
    return User(u['_id'])
